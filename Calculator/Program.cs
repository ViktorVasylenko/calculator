﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CalculatorManager_Console;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculatorManager manager = new CalculatorManager();
            manager.Run();
        }
    }
}
