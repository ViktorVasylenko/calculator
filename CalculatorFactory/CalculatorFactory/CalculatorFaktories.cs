﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComplexDigits;

namespace CalculatorFactories
{
    public interface TrigonometricOperations
    {
        Double Sin(Double Angle);
    }

    public abstract class BaseOperationsFactory_Real
    {
        public abstract string GetOperatinsType();

        public abstract Double Addition(Double Argument1, Double Argument2);
        public abstract Double Substraction(Double Argument1, Double Argument2);
        public abstract Double Multiplication(Double Argument1, Double Argument2);
        public abstract Double Division(Double Argument1, Double Argument2);
    }

    public abstract class EngineerOperationsFactory_Real: BaseOperationsFactory_Real
    {
        public abstract override string GetOperatinsType();

        public abstract Double Root(Double Base, Double Exponent);
        public abstract Double Reverse(Double Argument);
        public abstract Double PowerAny(Double Base, Double Exponent);
        public abstract Double Power2(Double Argument);
    }

    public abstract class BaseOperationsFactory_Complex
    {
        public abstract string GetOperatinsType();

        public abstract ComplexDigit Addition(ComplexDigit Argument1, ComplexDigit Argument2);
        public abstract ComplexDigit Substraction(ComplexDigit Argument1, ComplexDigit Argument2);
        public abstract ComplexDigit Multiplication(ComplexDigit Argument1, ComplexDigit Argument2);
        public abstract ComplexDigit Division(ComplexDigit Argument1, ComplexDigit Argument2);
    }
    
}
