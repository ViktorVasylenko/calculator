﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculatorFactories;
using ComplexDigits;

namespace CalculatorBusinessLogic
{
    /// <summary>
    /// Доступные типы базовых операций, выполняемых калькулятором
    /// </summary>
    public enum ExecutionMode
    {
        BaseMode_RealDigit = 1,
        EngineerMode_RealDigit = 2,
        
        BaseMode_ComplexDigit = 10
    };

    /// <summary>
    /// Доступные типы операций, выполняемых калькулятором с комплексными числами
    /// </summary>
    public enum OperationTypeComplex
    {
        Addition = 1,
        Substraction = 2,
        Multiple = 3,
        Division = 4
    };

    /// <summary>
    /// Доступные типы базовых операций, выполняемых калькулятором
    /// </summary>
    public enum OperationTypeBase
    {
        Addition = 1,
        Substraction = 2,
        Multiple = 3,
        Division = 4
    };

    /// <summary>
    /// Доступные типы операций для инженерного режима, выполняемых калькулятором
    /// </summary>
    public enum OperationTypeEngineer
    {
        Root = 20,
        Reverse = 21,
        PowerAny = 22,
        Power2 = 23
    };

    /// <summary>
    /// Статический класс для предоставления доступа к общим данным
    /// </summary>
    public static class Constants_Calculator
    {
        /// <summary>
        /// Разделитель типов операций в строке, возвращаемой вызовом CalculatorBL.GetOperationType
        /// </summary>
        public const string SEPARATOR_OperationType = ";";

    }

    /// <summary>
    /// Класс ошибки для случая выбора недопустимого/несуществующего режима работы калькулятора
    /// </summary>
    public class UnsupportedModeException : ApplicationException
    {
        public override string Message
        {
            get
            {
                return "Калькулятор не поддерживает указанного режима работы!";
            }
        }

        //Стандартные конструкторы
        public UnsupportedModeException() : base() { }
        public UnsupportedModeException(string message) : base(message) { }
        public UnsupportedModeException(string message, Exception innerException) : base(message, innerException) { }
    }

    /// <summary>
    /// Класс ошибки для случая выбора недопустимой/несуществующей операции
    /// </summary>
    public class NotOperationException : ApplicationException
    {
        public override string Message
        {
            get
            {
                return "Калькулятор не поддерживает выбранную операцию!";
            }
        }

        //Стандартные конструкторы
        public NotOperationException() : base() { }
        public NotOperationException(string message) : base(message) { }
        public NotOperationException(string message, Exception innerException) : base(message, innerException) { }
    }

    /// <summary>
    /// Класс ошибки для случая выхода за пределы допустимых значений в результате выполнения операции с числом двойной точности
    /// </summary>
    public class DoubleOverflowException : ApplicationException
    {
        public override string Message
        {
            get
            {
                return "Произошло переполнение допустимого значения! Полученный результат ошибочен! Необходимо изменить аргументы.";
            }
        }

        //Стандартные конструкторы
        public DoubleOverflowException() : base() { }
        public DoubleOverflowException(string message) : base(message) { }
        public DoubleOverflowException(string message, Exception innerException) : base(message, innerException) { }
    }

    /// <summary>
    /// Класс ошибки для случая выхода за пределы допустимых значений в действительной или мнимой частях в результате выполнения операции с комплексным числом
    /// </summary>
    public class ComplexDigitOverflowException : ApplicationException
    {
        public override string Message
        {
            get
            {
                return "Произошло переполнение допустимого значения в действительной или мнимой частях комплексного числа! Полученный результат ошибочен! Необходимо изменить аргументы.";
            }
        }

        //Стандартные конструкторы
        public ComplexDigitOverflowException() : base() { }
        public ComplexDigitOverflowException(string message) : base(message) { }
        public ComplexDigitOverflowException(string message, Exception innerException) : base(message, innerException) { }
    }

    /// <summary>
    /// Реализация экземпляра абстрактной фабрики по предоставлению базовых операций с комплексными числами
    /// </summary>
    internal class MakeOperations_ComplexDigit : BaseOperationsFactory_Complex
    {
        /// <summary>
        /// Операция сложения
        /// </summary>
        /// <param name="Argument1">Первый аргумент</param>
        /// <param name="Argument2">Второй аргумент</param>
        /// <returns>Число двойной точности</returns>
        public override ComplexDigit Addition(ComplexDigit Argument1, ComplexDigit Argument2)
        {
            ComplexDigit ret = new ComplexDigit(Double.NegativeInfinity, Double.NegativeInfinity);

            ret = Argument1 + Argument2;

            if (Double.IsInfinity(ret.RealPart)|| Double.IsInfinity(ret.ImaginePart))
            {
                ComplexDigitOverflowException exc = new ComplexDigitOverflowException();
                throw exc;
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Операция вычитания
        /// </summary>
        /// <param name="Argument1">Первый аргумент</param>
        /// <param name="Argument2">Второй аргумент</param>
        /// <returns>Число двойной точности</returns>
        public override ComplexDigit Substraction(ComplexDigit Argument1, ComplexDigit Argument2)
        {
            ComplexDigit ret = new ComplexDigit(Double.NegativeInfinity, Double.NegativeInfinity);

            ret = Argument1 - Argument2;

            if (Double.IsInfinity(ret.RealPart)|| Double.IsInfinity(ret.ImaginePart))
            {
                ComplexDigitOverflowException exc = new ComplexDigitOverflowException();
                throw exc;
            }
            else
            {
                return ret;
            }
        }
   
        /// <summary>
        /// Операция умножения
        /// </summary>
        /// <param name="Argument1">Первый аргумент</param>
        /// <param name="Argument2">Второй аргумент</param>
        /// <returns>Число двойной точности</returns>
        public override ComplexDigit Multiplication(ComplexDigit Argument1, ComplexDigit Argument2)
        {
            ComplexDigit ret = new ComplexDigit(Double.NegativeInfinity, Double.NegativeInfinity);

            ret = Argument1 * Argument2;

            if (Double.IsInfinity(ret.RealPart)|| Double.IsInfinity(ret.ImaginePart))
            {
                ComplexDigitOverflowException exc = new ComplexDigitOverflowException();
                throw exc;
            }
            else
            {
                return ret;
            }
        }
 
        /// <summary>
        /// Операция деления
        /// </summary>
        /// <param name="Argument1">Первый аргумент</param>
        /// <param name="Argument2">Второй аргумент</param>
        /// <returns>Число двойной точности</returns>
        public override ComplexDigit Division(ComplexDigit Argument1, ComplexDigit Argument2)
        {
            ComplexDigit ret = new ComplexDigit(Double.NegativeInfinity, Double.NegativeInfinity);

            ret = Argument1 / Argument2;

            if (Double.IsInfinity(ret.RealPart)|| Double.IsInfinity(ret.ImaginePart))
            {
                ComplexDigitOverflowException exc = new ComplexDigitOverflowException();
                throw exc;
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Возвращает список доступных операций в виде форматированной строки
        /// </summary>
        /// <returns>Строка вида "Val1 + SEPARATOR_OerationType + Val2 + SEPARATOR_OerationType + ValN"</returns>
        public override string GetOperatinsType()
        {
            string ret = "";

            ret += String.Format(OperationTypeComplex.Addition + "=  {0}" + Constants_Calculator.SEPARATOR_OperationType +
                                 OperationTypeComplex.Substraction + "=  {1}" + Constants_Calculator.SEPARATOR_OperationType +
                                 OperationTypeComplex.Multiple + "=  {2}" + Constants_Calculator.SEPARATOR_OperationType +
                                 OperationTypeComplex.Division + "=  {3}"
                                 ,
                                 (int)OperationTypeComplex.Addition,
                                 (int)OperationTypeComplex.Substraction,
                                 (int)OperationTypeComplex.Multiple,
                                 (int)OperationTypeComplex.Division
                                );

            return ret;
        }
    }

    /// <summary>
    /// Реализация экземпляра абстрактной фабрики по предоставлению базовых операций с действительными числами
    /// </summary>
    public class MakeOperations_Base : BaseOperationsFactory_Real
    {
        /// <summary>
        /// Возвращает список доступных операций в виде форматированной строки
        /// </summary>
        /// <returns>Строка вида "Val1 + SEPARATOR_OerationType + Val2 + SEPARATOR_OerationType + ValN"</returns>
        public override string GetOperatinsType()
        {
            string ret = "";

            ret += String.Format("Базовые операции:\n" +
                                 OperationTypeBase.Addition + "=  {0}" + Constants_Calculator.SEPARATOR_OperationType +
                                 OperationTypeBase.Substraction + "=  {1}" + Constants_Calculator.SEPARATOR_OperationType +
                                 OperationTypeBase.Multiple + "=  {2}" + Constants_Calculator.SEPARATOR_OperationType +
                                 OperationTypeBase.Division + "=  {3}"
                                 ,
                                 (int)OperationTypeBase.Addition,
                                 (int)OperationTypeBase.Substraction,
                                 (int)OperationTypeBase.Multiple,
                                 (int)OperationTypeBase.Division
                                );

            return ret;
        }

        /// <summary>
        /// Операция сложения
        /// </summary>
        /// <param name="Argument1">Первый аргумент</param>
        /// <param name="Argument2">Второй аргумент</param>
        /// <returns>Число двойной точности</returns>
        public override double Addition(double Argument1, double Argument2)
        {
            double ret = Double.NegativeInfinity;

            ret = Argument1 + Argument2;

            if (Double.IsInfinity(ret))
            {
                DoubleOverflowException exc = new DoubleOverflowException();
                throw exc;
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Операция вычитания
        /// </summary>
        /// <param name="Argument1">Первый аргумент</param>
        /// <param name="Argument2">Второй аргумент</param>
        /// <returns>Число двойной точности</returns>
        public override double Substraction(double Argument1, double Argument2)
        {
            double ret = Double.NegativeInfinity;

            ret = Argument1 - Argument2;

            if (Double.IsInfinity(ret))
            {
                DoubleOverflowException exc = new DoubleOverflowException();
                throw exc;
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Операция умножения
        /// </summary>
        /// <param name="Argument1">Первый аргумент</param>
        /// <param name="Argument2">Второй аргумент</param>
        /// <returns>Число двойной точности</returns>
        public override double Multiplication(double Argument1, double Argument2)
        {
            double ret = Double.NegativeInfinity;

            ret = Argument1 * Argument2;

            if (Double.IsInfinity(ret))
            {
                DoubleOverflowException exc = new DoubleOverflowException();
                throw exc;
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Операция деления
        /// </summary>
        /// <param name="Argument1">Первый аргумент</param>
        /// <param name="Argument2">Второй аргумент</param>
        /// <returns>Число двойной точности</returns>
        public override double Division(double Argument1, double Argument2)
        {
            double ret = Double.NegativeInfinity;

            ret = Argument1 / Argument2;

            if (Double.IsInfinity(ret))
            {
                DoubleOverflowException exc = new DoubleOverflowException();
                throw exc;
            }
            else
            {
                return ret;
            }
        }

    }

    /// <summary>
    /// Реализация экземпляра абстрактной фабрики по предоставлению базовых операций с действительными числами
    /// </summary>
    internal class MakeOperations_Engineer : EngineerOperationsFactory_Real
    {
        MakeOperations_Base baseOperationsFactory = new MakeOperations_Base();

        /// <summary>
        /// Возвращает список доступных операций в виде форматированной строки
        /// </summary>
        /// <returns>Строка вида "Val1 + SEPARATOR_OerationType + Val2 + SEPARATOR_OerationType + ValN"</returns>
        public override string GetOperatinsType()
        {
            string ret = "";

            ret = baseOperationsFactory.GetOperatinsType();

            ret += String.Format("\nРасширенные операции:\n" + 
                                 OperationTypeEngineer.Root + "=  {0}" + Constants_Calculator.SEPARATOR_OperationType +
                                 OperationTypeEngineer.Reverse + "=  {1}" + Constants_Calculator.SEPARATOR_OperationType +
                                 OperationTypeEngineer.PowerAny + "=  {2}" + Constants_Calculator.SEPARATOR_OperationType +
                                 OperationTypeEngineer.Power2 + "=  {3}"
                                 ,
                                 (int)OperationTypeEngineer.Root,
                                 (int)OperationTypeEngineer.Reverse,
                                 (int)OperationTypeEngineer.PowerAny,
                                 (int)OperationTypeEngineer.Power2
                                );

            return ret;
        }

        /// <summary>
        /// Операция сложения
        /// </summary>
        /// <param name="Argument1">Первый аргумент</param>
        /// <param name="Argument2">Второй аргумент</param>
        /// <returns>Число двойной точности</returns>
        public override double Addition(double Argument1, double Argument2)
        {
            return baseOperationsFactory.Addition(Argument1, Argument2);
        }

        /// <summary>
        /// Операция вычитания
        /// </summary>
        /// <param name="Argument1">Первый аргумент</param>
        /// <param name="Argument2">Второй аргумент</param>
        /// <returns>Число двойной точности</returns>
        public override double Substraction(double Argument1, double Argument2)
        {
            return baseOperationsFactory.Substraction(Argument1, Argument2);
        }

        /// <summary>
        /// Операция умножения
        /// </summary>
        /// <param name="Argument1">Первый аргумент</param>
        /// <param name="Argument2">Второй аргумент</param>
        /// <returns>Число двойной точности</returns>
        public override double Multiplication(double Argument1, double Argument2)
        {
            return baseOperationsFactory.Multiplication(Argument1, Argument2);
        }

        /// <summary>
        /// Операция деления
        /// </summary>
        /// <param name="Argument1">Первый аргумент</param>
        /// <param name="Argument2">Второй аргумент</param>
        /// <returns>Число двойной точности</returns>
        public override double Division(double Argument1, double Argument2)
        {
            return baseOperationsFactory.Division(Argument1, Argument2);
        }

        /// <summary>
        /// операция взятия корня указанной степени
        /// </summary>
        /// <param name="Base">Подкоренное значение</param>
        /// <param name="Exponent">Степень корня</param>
        /// <returns></returns>
        public override double Root(double Base, double Exponent)
        {
            double ret = Double.NegativeInfinity;

            ret = PowerAny(Base, Reverse(Exponent));

            if (Double.IsInfinity(ret))
            {
                DoubleOverflowException exc = new DoubleOverflowException();
                throw exc;
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Операция получение обратного числа (1/Х)
        /// </summary>
        /// <param name="Argument">Число, для которого надо найти обратное значение</param>
        /// <returns></returns>
        public override double Reverse(double Argument)
        {
            double ret = Double.NegativeInfinity;

            ret = 1 / Argument;

            if (Double.IsInfinity(ret))
            {
                DoubleOverflowException exc = new DoubleOverflowException();
                throw exc;
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Операция возведения числа в произвольную степень
        /// </summary>
        /// <param name="Base">Основание степени</param>
        /// <param name="Exponent">Показатель степени</param>
        /// <returns></returns>
        public override double PowerAny(double Base, double Exponent)
        {
            double ret = Double.NegativeInfinity;

            ret = Math.Pow(Base, Exponent);

            if (Double.IsInfinity(ret))
            {
                DoubleOverflowException exc = new DoubleOverflowException();
                throw exc;
            }
            else
            {
                return ret;
            }
        }

        /// <summary>
        /// Операция возведения в квадрат
        /// </summary>
        /// <param name="Argument">Число, которое надо возвести в квадрат</param>
        /// <returns></returns>
        public override double Power2(double Argument)
        {
            return PowerAny(Argument, 2);
        }
    }

    /// <summary>
    /// Класс, непосредственно реализующий операции калькулятора
    /// </summary>
    public class CalculatorBL
    {
        Int32 workMode = -1;

        /// <summary>
        /// Возвращает/устанавливает режим работы калькулятора
        /// </summary>
        public ExecutionMode WorkMode
        {
            get { return (ExecutionMode)workMode; }
            set
            {
                if (CheckAvailableWorkMode((Int32)value))
                {
                    workMode = (Int32)value;
                }
                else
                {
                    throw new UnsupportedModeException();
                }
            }
        }
        
        /// <summary>
        /// Возвращает список доступных режимов работы в виде форматированной строки
        /// </summary>
        /// <returns>Строка вида "Mode1 + SEPARATOR_OerationType + Mode2 + SEPARATOR_OerationType + ModeN"</returns>
        public string GetAvailableWorkMode()
        {
            string ret = "";

            ret += String.Format(ExecutionMode.BaseMode_RealDigit + "=  {0}" + Constants_Calculator.SEPARATOR_OperationType +
                                 ExecutionMode.EngineerMode_RealDigit + "=  {1}" + Constants_Calculator.SEPARATOR_OperationType +
                                 ExecutionMode.BaseMode_ComplexDigit + "=  {2}"
                                 ,
                                 (int)ExecutionMode.BaseMode_RealDigit,
                                 (int)ExecutionMode.EngineerMode_RealDigit,
                                 (int)ExecutionMode.BaseMode_ComplexDigit
                                );

            return ret;
        }

        /// <summary>
        /// Проверяет существование режима работы калькулятора с заданным кодом
        /// </summary>
        /// <param name="WorkModeCode">Код проверяемого режима работы</param>
        /// <returns>
        /// true - режим существует
        /// false - режима не существует</returns>
        public bool CheckAvailableWorkMode(int WorkModeCode)
        {
            Boolean res = false;

            switch (WorkModeCode)
            {
                case (Int32)ExecutionMode.BaseMode_RealDigit:
                    res = true;
                    break;
                case (Int32)ExecutionMode.EngineerMode_RealDigit:
                    res = true;
                    break;
                case (Int32)ExecutionMode.BaseMode_ComplexDigit:
                    res = true;
                    break;
                default:
                    res = false;
                    break;
            };

            return res;
        }

        /// <summary>
        /// Возвращает список доступных операций в виде форматированной строки
        /// </summary>
        /// <returns>Строка вида "Val1 + SEPARATOR_OerationType + Val2 + SEPARATOR_OerationType + ValN"</returns>
        public string GetOperationType()
        {
            string ret = "";

            switch(workMode)
            {
                case (int)ExecutionMode.BaseMode_RealDigit:
                    MakeOperations_Base fab = new MakeOperations_Base();
                    ret = fab.GetOperatinsType();
                    break;
                case (int)ExecutionMode.EngineerMode_RealDigit:
                    MakeOperations_Engineer fab1 = new MakeOperations_Engineer();
                    ret = fab1.GetOperatinsType();
                    break;
                case (int)ExecutionMode.BaseMode_ComplexDigit:
                    MakeOperations_ComplexDigit fab2 = new MakeOperations_ComplexDigit();
                    ret = fab2.GetOperatinsType();
                    break;
            };

            return ret;
        }

        /// <summary>
        /// Проверяет существование операции с заданным кодом
        /// </summary>
        /// <param name="OperationCode">Код проверяемой операции</param>
        /// <returns>
        /// true - операция существует
        /// false - операции не существует</returns>
        public bool CheckAvailableOperation(int OperationCode)
        {
            bool res = false;

            switch (workMode)
            {
                case (int)ExecutionMode.BaseMode_RealDigit:
                    res = CheckAvailableOperation_RealBase(OperationCode);
                    break;
                case (int)ExecutionMode.EngineerMode_RealDigit:
                    res = CheckAvailableOperation_RealEngineer(OperationCode);
                    break;
                case (int)ExecutionMode.BaseMode_ComplexDigit:
                    res = CheckAvailableOperation_ComlexDigitBase(OperationCode);
                    break;
                default:
                    res = false;
                    break;
            };

            return res;
        }

        /// <summary>
        /// Проверяет существование операции с заданным кодом в списке базовых операций
        /// </summary>
        /// <param name="OperationCode">Код проверяемой операции</param>
        /// <returns>
        /// true - операция существует
        /// false - операции не существует</returns>
        private bool CheckAvailableOperation_RealBase(int OperationCode)
        {
            bool res = false;

            switch (OperationCode)
            {
                case (int)OperationTypeBase.Addition:
                    res = true;
                    break;
                case (int)OperationTypeBase.Substraction:
                    res = true;
                    break;
                case (int)OperationTypeBase.Multiple:
                    res = true;
                    break;
                case (int)OperationTypeBase.Division:
                    res = true;
                    break;
                default:
                    res = false;
                    break;
            };

            return res;
        }

        /// <summary>
        /// Проверяет существование операции с заданным кодом в списке инженерных операций
        /// </summary>
        /// <param name="OperationCode">Код проверяемой операции</param>
        /// <returns>
        /// true - операция существует
        /// false - операции не существует</returns>
        private bool CheckAvailableOperation_RealEngineer(int OperationCode)
        {
            bool baseRes = false;
            bool mainRes = false;

            // Проверяем на соответствие подмножеству простых операций
            baseRes = CheckAvailableOperation_RealBase(OperationCode);
            if (baseRes == true)
                return baseRes;

            // Проверяем на соответствие подмножеству инженерных операций
            switch (OperationCode)
            {
                case (int)OperationTypeEngineer.Root:
                    mainRes = true;
                    break;
                case (int)OperationTypeEngineer.Reverse:
                    mainRes = true;
                    break;
                case (int)OperationTypeEngineer.PowerAny:
                    mainRes = true;
                    break;
                case (int)OperationTypeEngineer.Power2:
                    mainRes = true;
                    break;
                default:
                    mainRes = false;
                    break;
            };

            return mainRes;
        }

        /// <summary>
        /// Проверяет существование операции с заданным кодом в списке базовых операций с комплексными числами
        /// </summary>
        /// <param name="OperationCode">Код проверяемой операции</param>
        /// <returns>
        /// true - операция существует
        /// false - операции не существует</returns>
        private bool CheckAvailableOperation_ComlexDigitBase(int OperationCode)
        {
            bool res = false;

            switch (OperationCode)
            {
                case (int)OperationTypeComplex.Addition:
                    res = true;
                    break;
                case (int)OperationTypeComplex.Substraction:
                    res = true;
                    break;
                case (int)OperationTypeComplex.Multiple:
                    res = true;
                    break;
                case (int)OperationTypeComplex.Division:
                    res = true;
                    break;
                default:
                    res = false;
                    break;
            };

            return res;
        }

        /// <summary>
        /// Запускает через соответствующую фабрику операцию с заданным кодом и аргументами
        /// </summary>
        /// <param name="OperationCode">Код операции</param>
        /// <param name="Argument1">Аргумент 1</param>
        /// <param name="Argument2">Аргумент 2</param>
        /// <returns></returns>
        public Double Execute(Int32 OperationCode, Double Argument1, Double Argument2)
        {
            Double ret = 0;

            switch(workMode)
            {
                //
                case (int)ExecutionMode.BaseMode_RealDigit:
                    //
                    MakeOperations_Base fabBase = new MakeOperations_Base();
                    //
                    switch(OperationCode)
                    {
                        case (Int32)OperationTypeBase.Addition:
                            ret = fabBase.Addition(Argument1, Argument2);
                            break;
                        case (Int32)OperationTypeBase.Substraction:
                            ret = fabBase.Substraction(Argument1, Argument2);
                            break;
                        case (Int32)OperationTypeBase.Multiple:
                            ret = fabBase.Multiplication(Argument1, Argument2);
                            break;
                        case (Int32)OperationTypeBase.Division:
                            ret = fabBase.Division(Argument1, Argument2);
                            break;
                        default:
                            throw new NotOperationException();
                    }
                    break;
                //
                case (int)ExecutionMode.EngineerMode_RealDigit:
                    //
                    MakeOperations_Engineer fabEng = new MakeOperations_Engineer();
                    //
                    switch (OperationCode)
                    {
                        case (Int32)OperationTypeBase.Addition:
                            ret = fabEng.Addition(Argument1, Argument2);
                            break;
                        case (Int32)OperationTypeBase.Substraction:
                            ret = fabEng.Substraction(Argument1, Argument2);
                            break;
                        case (Int32)OperationTypeBase.Multiple:
                            ret = fabEng.Multiplication(Argument1, Argument2);
                            break;
                        case (Int32)OperationTypeBase.Division:
                            ret = fabEng.Division(Argument1, Argument2);
                            break;

                        case (Int32)OperationTypeEngineer.Root:
                            ret = fabEng.Root(Argument1, Argument2);
                            break;
                        case (Int32)OperationTypeEngineer.Reverse:
                            ret = fabEng.Reverse(Argument1);
                            break;
                        case (Int32)OperationTypeEngineer.PowerAny:
                            ret = fabEng.PowerAny(Argument1, Argument2);
                            break;
                        case (Int32)OperationTypeEngineer.Power2:
                            ret = fabEng.Power2(Argument1);
                            break;
                        default:
                            throw new NotOperationException();
                    }
                    break;
            }

            return ret;
        }

        /// <summary>
        /// Запускает через соответствующую фабрику операцию с заданным кодом и аргументами
        /// </summary>
        /// <param name="OperationCode">Код операции</param>
        /// <param name="Argument1">Аргумент 1</param>
        /// <param name="Argument2">Аргумент 2</param>
        /// <returns></returns>
        public ComplexDigit Execute(Int32 OperationCode, ComplexDigit Argument1, ComplexDigit Argument2)
        {
            ComplexDigit ret = new ComplexDigit(0, 0);
            switch (workMode)
            {
                //
                case (int)ExecutionMode.BaseMode_ComplexDigit:
                    //
                    MakeOperations_ComplexDigit fabComplex = new MakeOperations_ComplexDigit();
                    //
                    switch (OperationCode)
                    {
                        case (Int32)OperationTypeComplex.Addition:
                            ret = fabComplex.Addition(Argument1, Argument2);
                            break;
                        case (Int32)OperationTypeComplex.Substraction:
                            ret = fabComplex.Substraction(Argument1, Argument2);
                            break;
                        case (Int32)OperationTypeComplex.Multiple:
                            ret = fabComplex.Multiplication(Argument1, Argument2);
                            break;
                        case (Int32)OperationTypeComplex.Division:
                            ret = fabComplex.Division(Argument1, Argument2);
                            break;
                        default:
                            throw new NotOperationException();
                    }
                    break;
            }
            return ret;
        }

    }
}
