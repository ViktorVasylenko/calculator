﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculatorBusinessLogic;
using ComplexDigits;

namespace CalculatorManager_Console
{
    /// <summary>
    /// Класс - менеджер для управления калькулятором в консольной среде
    /// </summary>
    public class CalculatorManager
    {
        CalculatorBL calc;

        Double Arg1, Arg2;
        ComplexDigit complexArg1, complexArg2;
        Double result = Double.NegativeInfinity;
        ComplexDigit complexResult = new ComplexDigit(Double.NegativeInfinity, Double.NegativeInfinity);
        Int32 curOperationType = -1;
        Int32 workMode = -1;

        public CalculatorManager()
        {
            calc = new CalculatorBL();
        }

        /// <summary>
        /// Основной метод для организации выполнения работы калькулятора
        /// </summary>
        public void Run()
        {
            // Оповещение о списке возможных режимов работы калькулятора
            WriteAvailableWorkMode();
            // Получаем и обрабатываем тип режима работы от оператора
            workMode = ReadWorkMode();
            if (calc.CheckAvailableWorkMode(workMode) == false)
            {
                Console.WriteLine("Выбранный режим работы калькулятора не поддерживается!");
                Console.ReadKey(true);
                return;
            }
            // Устанавливаем режим работы для экземпляра калькулятора
            try
            {
                calc.WorkMode = (ExecutionMode)workMode;
            }
            catch (UnsupportedModeException exc)
            {
                Console.WriteLine(exc.Message);
                return;
            }

            // Оповещение о списке возможных операций
            WriteAvailableOperationsType();

            // Получаем и обрабатываем тип операции от оператора
            curOperationType = ReadOperationType();
            if (calc.CheckAvailableOperation(curOperationType) == false)
            {
                Console.WriteLine("Выбранный тип операции не поддерживается!");
                Console.ReadKey(true);
                return;
            }

            // Получаем аргументы от оператора
            switch((ExecutionMode)workMode)
            {
                case ExecutionMode.BaseMode_ComplexDigit:
                    complexArg1 = ReadComplexArgument(1);
                    complexArg2 = ReadComplexArgument(2);
                    break;
                case ExecutionMode.BaseMode_RealDigit:
                    Arg1 = ReadRealArgument(1);
                    Arg2 = ReadRealArgument(2);
                    break;
                case ExecutionMode.EngineerMode_RealDigit:
                    Arg1 = ReadRealArgument(1);
                    Arg2 = ReadRealArgument(2);
                    break;
            }

            // Непосредственно проводим вычисления
            try
            {
                switch ((ExecutionMode)workMode)
                {
                    case ExecutionMode.BaseMode_ComplexDigit:
                        complexResult = calc.Execute(curOperationType, complexArg1, complexArg2);
                        break;
                    case ExecutionMode.BaseMode_RealDigit:
                        result = calc.Execute(curOperationType, Arg1, Arg2);
                        break;
                    case ExecutionMode.EngineerMode_RealDigit:
                        result = calc.Execute(curOperationType, Arg1, Arg2);
                        break;
                }
            }
            catch (DoubleOverflowException exc)
            {
                Console.WriteLine(exc.Message);
            }
            catch (ComplexDigitOverflowException exc)
            {
                Console.WriteLine(exc.Message);
            }
            catch (NotOperationException exc)
            {
                Console.WriteLine(exc.Message);
            }
            catch
            {
                Console.WriteLine("Произошла непредвиденная ошибка на этапе выполнения операции! Обратитесь к разработчику.");
            }

            // Отображаем полученные результаты
            switch ((ExecutionMode)workMode)
            {
                case ExecutionMode.BaseMode_ComplexDigit:
                    Console.WriteLine("Результат = {0}", complexResult.ToString());
                    break;
                case ExecutionMode.BaseMode_RealDigit:
                    Console.WriteLine("Результат = {0}", result);
                    break;
                case ExecutionMode.EngineerMode_RealDigit:
                    Console.WriteLine("Результат = {0}", result);
                    break;
            }
            Console.ReadKey(true);
        }

        /// <summary>
        /// Выводит в консоль список возможных режимов работы калькулятора
        /// </summary>
        private void WriteAvailableWorkMode()
        {
            string workModeList = "Список доступных режимов работы калькулятора:\n";
            string[] tmp;

            // Разделяем строку со всеми типами операций по символу разделителя
            tmp = calc.GetAvailableWorkMode().Split(new string[] { Constants_Calculator.SEPARATOR_OperationType }, StringSplitOptions.RemoveEmptyEntries);

            // Формируем вид строки для вывода в консоль
            foreach (string s in tmp)
            {
                workModeList += s + "\n";
            }

            Console.WriteLine(workModeList);
        }

        /// <summary>
        /// Получает режим работы калькулятора от оператора
        /// </summary>
        /// <returns>
        /// Код операции
        /// </returns>
        private int ReadWorkMode()
        {
            int workMode = -1;
            string answer;
            bool done = true;

            // Повторяем запрос режима работы до тех пор, пока он не будет введен правильно
            do
            {
                Console.WriteLine("Введите режим работы калькулятора...");
                answer = Console.ReadLine();

                if (Int32.TryParse(answer, out workMode) == false)
                {
                    Console.WriteLine("Для определения режима работы допускаются только целые числа! Повторите ввод.");
                    done = false;
                }
                else
                {
                    done = true;
                }
            }
            while (done == false);

            //
            return workMode;
        }
        
        /// <summary>
        /// Получает тип операции от оператора
        /// </summary>
        /// <returns>
        /// Код операции
        /// </returns>
        private int ReadOperationType()
        {
            int opType = -1;
            string answer;
            bool done = true;

            // Повторяем запрос кода операции до тех пор, пока он не будет введен правильно
            do
            {
                Console.WriteLine("Введите код операции...");
                answer = Console.ReadLine();

                if (Int32.TryParse(answer, out opType) == false)
                {
                    Console.WriteLine("Для определения типа операции допускаются только целые числа! Повторите ввод.");
                    done = false;
                }
                else
                {
                    done = true;
                }
            }
            while (done == false);
            
            //
            return opType;
        }

        /// <summary>
        /// Получает значение аргумента от оператора (действительные числа)
        /// </summary>
        /// <returns>Значение аргумента</returns>
        private double ReadRealArgument(int ArgumentSerialNumber)
        {
            double argValue = Double.PositiveInfinity;
            string answer;
            bool done = true;

            // Повторяем запрос аргумента до тех пор, пока он не будет введен правильно
            do
            {
                Console.WriteLine("Введите значение аргумента №{0}", ArgumentSerialNumber.ToString());
                answer = Console.ReadLine();

                if (Double.TryParse(answer, out argValue) == false)
                {
                    Console.WriteLine("Для определения аргумента допускаются только числа! Проверьте правильность ввода символа разделителя целой и дробной части числа! Повторите ввод.");
                    done = false;
                }
                else
                {
                    done = true;
                }
            }
            while (done == false);

            //
            return argValue;
        }

        /// <summary>
        /// Получает значение аргумента от оператора (комплексные числа)
        /// </summary>
        /// <returns>Значение аргумента</returns>
        private ComplexDigit ReadComplexArgument(int ArgumentSerialNumber)
        {
            double argValueReal = Double.PositiveInfinity;
            double argValueImagine = Double.PositiveInfinity;
            string answer;
            bool done = true;

            // Повторяем запрос действительной части аргумента до тех пор, пока он не будет введен правильно
            do
            {
                Console.WriteLine("Введите значение действительной части аргумента №{0}", ArgumentSerialNumber.ToString());
                answer = Console.ReadLine();

                if (Double.TryParse(answer, out argValueReal) == false)
                {
                    Console.WriteLine("Для определения действительной части аргумента допускаются только числа! Проверьте правильность ввода символа разделителя целой и дробной части числа! Повторите ввод.");
                    done = false;
                }
                else
                {
                    done = true;
                }
            }
            while (done == false);

            // Повторяем запрос мнимой части аргумента до тех пор, пока он не будет введен правильно
            do
            {
                Console.WriteLine("Введите значение мнимой части аргумента №{0}", ArgumentSerialNumber.ToString());
                answer = Console.ReadLine();

                if (Double.TryParse(answer, out argValueImagine) == false)
                {
                    Console.WriteLine("Для определения мнимой части аргумента допускаются только числа! Проверьте правильность ввода символа разделителя целой и дробной части числа! Повторите ввод.");
                    done = false;
                }
                else
                {
                    done = true;
                }
            }
            while (done == false);

            //
            return new ComplexDigit(argValueReal, argValueImagine);
        }

        /// <summary>
        /// Выводит в консоль список возможных операций
        /// </summary>
        private void WriteAvailableOperationsType()
        {
            string operationsList = "Список доступных операций:\n";
            string[] tmp;

            // Разделяем строку со всеми типами операций по символу разделителя
            tmp = calc.GetOperationType().Split(new string[] { Constants_Calculator.SEPARATOR_OperationType }, StringSplitOptions.RemoveEmptyEntries);

            // Формируем вид строки для вывода в консоль
            foreach (string s in tmp)
            {
                operationsList += s + "\n";
            }

            Console.WriteLine(operationsList);
        }

    }
}
