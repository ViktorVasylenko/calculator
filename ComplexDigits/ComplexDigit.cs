﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComplexDigits
{
    /// <summary>
    /// Представляет комплексное число и базовые арифметические операции с ним.
    /// </summary>
    public class ComplexDigit
    {
        Double real;
        Double imagine;

        /// <summary>
        /// Конструктор нового комплексного числа
        /// </summary>
        /// <param name="RealPart">Реальная часть комплексного числа</param>
        /// <param name="ImaginePart">Мнимаю часть комплексного числа</param>
        public ComplexDigit(Double RealPart, Double ImaginePart)
        {
            real = RealPart;
            imagine = ImaginePart;
        }

        /// <summary>
        /// Возвращает/устанавливает реальную часть комплексного числа
        /// </summary>
        public Double RealPart
        {
            get { return real; }
            set { real = value; }
        }

        /// <summary>
        /// Возвращает/устанавливает мнимую часть комплексного числа
        /// </summary>
        public Double ImaginePart
        {
            get { return imagine; }
            set { imagine = value; }
        }

        /// <summary>
        /// Выполняет сложение двух комплексных чисел
        /// </summary>
        /// <param name="a">Первое слагаемое</param>
        /// <param name="b">Второе слагаемое</param>
        /// <returns></returns>
        public static ComplexDigit operator +(ComplexDigit a, ComplexDigit b)
        {
            ComplexDigit result;
            Double resRealPart = 0;
            Double resImaginePart = 0;

            resRealPart = a.real + b.real;
            resImaginePart = a.imagine + b.imagine;

            result = new ComplexDigit(resRealPart, resImaginePart);
            return result;
        }

        /// <summary>
        /// Выполняет вычитание двух комплексных чисел
        /// </summary>
        /// <param name="a">Первый аргумент</param>
        /// <param name="b">Второй аргумент</param>
        /// <returns></returns>
        public static ComplexDigit operator -(ComplexDigit a, ComplexDigit b)
        {
            ComplexDigit result;
            Double resRealPart = 0;
            Double resImaginePart = 0;

            resRealPart = a.real - b.real;
            resImaginePart = a.imagine - b.imagine;

            result = new ComplexDigit(resRealPart, resImaginePart);
            return result;
        }

        /// <summary>
        /// Выполняет умножение двух комплексных чисел
        /// </summary>
        /// <param name="a">Первый множитель</param>
        /// <param name="b">Второй множитель</param>
        /// <returns></returns>
        public static ComplexDigit operator *(ComplexDigit a, ComplexDigit b)
        {
            ComplexDigit result;
            Double resRealPart = 0;
            Double resImaginePart = 0;

            resRealPart = a.real * b.real - a.imagine * b.imagine;
            resImaginePart = a.real * b.imagine + b.real * a.imagine;

            result = new ComplexDigit(resRealPart, resImaginePart);
            return result;
        }

        /// <summary>
        /// Выполняет деление двух комплексных чисел
        /// </summary>
        /// <param name="a">Делимое</param>
        /// <param name="b">Делитель</param>
        /// <returns></returns>
        public static ComplexDigit operator /(ComplexDigit a, ComplexDigit b)
        {
            ComplexDigit result;
            Double resRealPart = 0;
            Double resImaginePart = 0;

            resRealPart = (a.real * b.real + a.imagine * b.imagine) / (b.real * b.real + b.imagine * b.imagine);
            resImaginePart = (b.real * a.imagine - a.real * b.imagine) / (b.real * b.real + b.imagine * b.imagine);

            result = new ComplexDigit(resRealPart, resImaginePart);
            return result;
        }

        /// <summary>
        /// Выводит строкое представление комплексного числа
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (this.imagine >= 0)
            {
                return String.Format("{0}+i{1}", this.real.ToString(), this.imagine.ToString());
            }
            else
            {
                return String.Format("{0}-i{1}", this.real.ToString(), Math.Abs(this.imagine).ToString());
            }
            
        }
    }
}
